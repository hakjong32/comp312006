#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <pthread.h>

#define MAX_CMDS 100
#define MAX_CMDLEN 2048
#define MAX_DATAS 100
#define MAX_DATALEN 1025
#define MAX_ARGS 100
#define MAX_ARGLEN 1024


struct data_t
{
	char q[MAX_DATAS][MAX_DATALEN];
	pthread_mutex_t lock;
	pthread_cond_t cond;
	int now;
	int next;
	int isfull;
	int tnum;
};

char cmdlist[MAX_CMDS][MAX_CMDLEN];
int cmdlist_len = 0;

pthread_mutex_t lock_fd;

void parse_cmd(char *args[MAX_ARGS], char *cmd)
{
	int in_q = 0;
	int is_q_double;
	int is_start = 1;
	int args_idx = 0;
	int cmd_len = strlen(cmd);
	int i;

	for (i = 0; i < cmd_len; ++i)
	{
		switch (cmd[i])
		{
			case ' ':
				if (!in_q)
				{
					is_start = 1;
					cmd[i] = '\0';
				}
				break;
			case '\"':
			case '\'':
				if (!in_q) {
					is_start = 1;
					is_q_double = (cmd[i] == '\"' ? 1 : 0);
					cmd[i] = '\0';
					in_q = 1;
				}
				else if ((cmd[i] == '\"' && is_q_double)
						|| (cmd[i] == '\'' && !is_q_double))
				{
					is_start = 1;
					cmd[i] = '\0';
					in_q = 0;
				}
				break;
			default:
				if (is_start)
				{
					args[args_idx++] = cmd + i;
					is_start = 0;
				}
				break;
		}
	}
	args[args_idx++] = NULL;
}


void *t_func(void *_data)
{
	struct data_t *data = (struct data_t*) _data;
	int p1[2];
	int i;
	int rtn;

	pthread_mutex_lock(&lock_fd);
	if (pipe(p1) == -1)
	{
		fprintf(stderr, "%d: pipe error\n", data->tnum);
		exit(1);
	}

	if (fork() == 0)
	{
		char *args[MAX_ARGS];
		close(p1[1]);
		dup2(p1[0], STDIN_FILENO);
		close(p1[0]);
		char buf[BUFSIZ] = "cat";

		args[0] = buf;

		// 재귀하여 여러 명령 처리
		for (i = 1; i < cmdlist_len; ++i)
		{
			int p2[2];

			if (pipe(p2) == -1)
			{
				fprintf(stderr, "%d: pipe error\n", data->tnum);
				exit(1);
			}
			
			// 자식 프로세스 
			if (fork() == 0)
			{
				close(p2[0]);
				dup2(p2[1], STDOUT_FILENO);
				close(p2[1]);
				break;
			}

			close(p2[1]);
			dup2(p2[0], STDIN_FILENO);
			close(p2[0]);
		}
		
		parse_cmd(args, cmdlist[i - 1]);
		execvp(args[0], args);
	}
	
	close(p1[0]);
	
	pthread_mutex_unlock(&lock_fd);

	
	pthread_mutex_t *lock = &data->lock;
	pthread_cond_t *cond = &data->cond;

	while (1)
	{
		char str[MAX_DATALEN];

		pthread_mutex_lock(lock);

		// 비었을 경우 신호 올 때 까지 기다림
		if (!(data->isfull) && data->next == data->now)
		{
			pthread_cond_wait(cond, lock);
		}
		// 문장 하나 가져오기
		strcpy(str, data->q[data->now++]);
		data->now %= MAX_DATAS;
		data->isfull = 0;

		pthread_mutex_unlock(lock);

		// EOF면 종료
		if (str[0] == EOF)
		{
			close(p1[1]);
			wait(&rtn);
			return NULL;
		}

		// 문장 하나 처리
		write(p1[1], str, sizeof(char) * strlen(str));
	}
}


int main(int argc, char *argv[])
{
	int num_thread = 1;

	char cmd_str[MAX_DATALEN] = "";

	// 스레드용
	pthread_t *p_thread;
	struct data_t *p_data;

	char *now_pos = NULL;
	int i;
	void *nptr = NULL;

	pthread_mutex_init(&lock_fd, NULL);

	// 명령 인자 처리
	for (i = 1; i < argc; ++i)
	{
		switch(argv[i][0])
		{
			case '-': // option
				if (strlen(argv[i]) > 1)
				{
					switch(argv[i][1])
					{
						case 'c':
							num_thread = atoi(argv[++i]);
							break;

						default:
							fprintf(stderr, "invalid option: %s\n", argv[i]);
							exit(1);
							break;
					}
				}
				else
				{
					fprintf(stderr, "invalid argument\n");
					exit(1);
				}
				break;

			default:
				strcpy(cmd_str, argv[i]);
				break;
		}
	}

	// 스레드 수가 1보다 작을 때
	if (num_thread < 1)
	{
		fprintf(stderr, "thread num(-c [NUM]) must be larger then 0\n");
		exit(1);
	}

	// 실행 할 명령이 없을 때
	if (strlen(cmd_str) == 0)
	{
		fprintf(stderr, "usuage: papi -c [number of threads] command\n");
		exit(1);
	}

	// command 파싱
	now_pos = cmd_str;
	while (1)
	{
		int idx = 0;
		char *next_pos = strstr(now_pos, "==");

		// 첫 공백 제거
		if (*now_pos == ' ')
			now_pos++;

		// command 하나 복사
		while (*now_pos != '\0' && now_pos != next_pos)
			cmdlist[cmdlist_len][idx++] = *(now_pos++);
		cmdlist[cmdlist_len++][idx++] = '\0'; 

		// '==' 통과
		if (*now_pos != '\0')
			now_pos += 2;

		// 마지막 도달 시 나감
		if (next_pos == NULL)
			break;
	}


	// 스레드 준비
	p_thread = (pthread_t*) malloc(sizeof(pthread_t) * num_thread);
	p_data = (struct data_t*) malloc(sizeof(struct data_t) * num_thread);
	
	for (i = 0; i < num_thread; ++i)
	{
		p_data[i].now = 0;
		p_data[i].next = 0;
		p_data[i].isfull = 0;
		p_data[i].tnum = i;
		pthread_mutex_init(&p_data[i].lock, NULL);
		pthread_cond_init(&p_data[i].cond, NULL);

		pthread_create(&p_thread[i], NULL, t_func, (void*) &p_data[i]);
	}

	// stdin 끝까지 읽어오기
	i = -1; // 1 더할것이므로 0으로 시작
	while (!feof(stdin))
	{
		char str[MAX_DATALEN];

		// 한줄 받음
		if (fgets(str, MAX_DATALEN, stdin) == NULL)
			break;

		// 한 줄씩 처리
		while (1)
		{
			i = (i + 1) % num_thread;
			
			pthread_mutex_lock(&p_data[i].lock);
			
			// 꽉찼으면 통과
			if (p_data[i].isfull)
			{
				pthread_mutex_unlock(&p_data[i].lock);
				continue;
			}

			strcpy(p_data[i].q[p_data[i].next++], str);
			p_data[i].next %= MAX_DATAS;
			// 채운 후 꽉찼으면 표시
			if (p_data[i].next == p_data[i].now)
				p_data[i].isfull = 1;

			// 채운 후 signal
			pthread_cond_signal(&p_data[i].cond);
			pthread_mutex_unlock(&p_data[i].lock);
			break;
		}
	}


	// send EOF and join
	for (i = 0; i < num_thread; ++i)
	{
		char str[2];
		str[0] = EOF;
		str[1] = '\0';

		// full이 아님을 보장
		pthread_mutex_lock(&p_data[i].lock);
		while (p_data[i].isfull)
		{
			pthread_mutex_unlock(&p_data[i].lock);
			pthread_mutex_lock(&p_data[i].lock);
		} 	
		
		strcpy(p_data[i].q[p_data[i].next++], str);
		p_data[i].next %= MAX_DATAS;

		// 채운 후 꽉찼으면 표시
		if (p_data[i].next == p_data[i].now)
			p_data[i].isfull = 1;

		// 채운 후 signal
		pthread_cond_signal(&p_data[i].cond);
		pthread_mutex_unlock(&p_data[i].lock);
	}

	for (i = 0; i < num_thread; ++i)
	{
		// join
		pthread_join(p_thread[i], &nptr);
	}


	
	// 종료
	return 0;
}
